import React, { Component } from "react";
import ListFri from "./listfri";
import Message from "./mess";
import AddFri from "./addfri"
export default class Home extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { listfri } = this.props;
    return (
      <div className="ui">
      <AddFri/>
        <ListFri listfri={listfri} />
        <div className="chat">
          <div className="top">
            <div className="avatar">
              <img
                width="50"
                height="50"
                src="http://cs625730.vk.me/v625730358/1126a/qEjM1AnybRA.jpg"
              />
            </div>
            <div className="info">
              <div className="name">Юния Гапонович</div>
              <div className="count">already 1 902 messages</div>
            </div>
            <i class="fa fa-user-plus" />
          </div>
          <Message />
          <div className="write-form">
            <textarea
              placeholder="Type your message"
              name="e"
              id="texxt"
              rows="2"
            />
            <i className="fa fa-picture-o" />
            <i className="fa fa-file-o" />
            <span className="send">Send</span>
          </div>
        </div>
      </div>
    );
  }
}
