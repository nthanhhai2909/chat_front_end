import React, { Component } from "react";
import { Modal, Button } from "react-bootstrap";
export default class AddFri extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Modal show={true}>
          <Modal.Header>
            <Modal.Title>Friend requests</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="container-fluid friend">
              <div className="row">
                <div className="col-md-8">
                  <p className="name">Name</p>
                  <p className="email">Email</p>
                </div>
                <div className="col-md-4">
                  <button>Confirm</button>
                </div>
              </div>
            </div>
          </Modal.Body>

          <Modal.Header>
            <Modal.Title>Search friend</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <input type="text" placeholder="Enter email, name" />
            <div className="container-fluid friend">
              <div className="row">
                <div className="col-md-8">
                  <p className="name">Name</p>
                  <p className="email">Email</p>
                </div>
                <div className="col-md-4">
                  <button>Add friend</button>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
    );
  }
}
