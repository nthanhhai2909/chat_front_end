import React, { Component } from "react";

export default class ListFri extends Component {
  constructor(props) {
    super(props);
  }

  render() {
      const {listfri} = this.props;
    return (
      <div className="left-menu">
        <form action="#" className="search">
          <input placeholder="search..." type="search" name="" id="" />
        </form>
        <menu className="list-friends">
          <li>
            <img
              width="50"
              height="50"
              src="http://cs625730.vk.me/v625730358/1126a/qEjM1AnybRA.jpg"
            />
            <div className="info">
              <div className="user">Юния Гапонович</div>
              <div className="status on"> online</div>
            </div>
          </li>
        </menu>
      </div>
    );
  }
}
