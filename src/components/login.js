import React, { Component } from "react";
import "font-awesome/css/font-awesome.min.css";
import { validateEmail } from "../utils/validation";
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  submit() {
    if (!validateEmail(this.state.email)) {
      this.setState({ information: "Email invalid" });
      return;
    }

    this.props.submit(this.state);
  }
  render() {
    return (
      <div className="main">
        <section class="sign-in">
          <div class="container">
            <div class="signin-content">
              <div class="signin-image">
                <figure>
                  <img
                    src={
                      process.env.PUBLIC_URL + "/assets/img/signin-image.jpg"
                    }
                  />
                </figure>
                <a href="#" class="signup-image-link">
                  Create an account
                </a>
              </div>
              <div class="signin-form">
                <h2 class="form-title">Login</h2>
                <h3>{this.props.res}</h3>
                <div method="POST" class="register-form" id="login-form">
                  <div class="form-group">
                    <label for="your_name">
                      <i class="fa fa-envelope-o" aria-hidden="true" />
                    </label>
                    <input
                      type="text"
                      name="your_name"
                      id="your_name"
                      placeholder="Your Name"
                      value={this.state.email}
                      onChange={event =>
                        this.setState({ email: event.target.value })
                      }
                    />
                  </div>
                  <div class="form-group">
                    <label for="your_pass">
                      <i class="fa fa-key" aria-hidden="true" />
                    </label>
                    <input
                      type="password"
                      name="your_pass"
                      id="your_pass"
                      placeholder="Password"
                      value={this.state.password}
                      onChange={event =>
                        this.setState({ password: event.target.value })
                      }
                    />
                  </div>
                  <div class="form-group form-button">
                    <input
                      type="submit"
                      name="signin"
                      id="signin"
                      class="form-submit"
                      value="Log in"
                      onClick={() => this.submit()}
                    />
                  </div>
                </div>
                <div class="social-login">
                  <span class="social-label">Or login with</span>
                  <ul class="socials">
                    <i class="fa fa-facebook fb" aria-hidden="true" />
                    <i class="fa fa-google" aria-hidden="true" />
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Login;
