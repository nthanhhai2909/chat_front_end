import { USER } from "../contants/contants";
import { combineReducers } from "redux";

const login = (state = { login: false }, action) => {
  switch (action.type) {
    case USER.LOGIN_SUCCESS:
      return { ...state, login: true };
    case USER.LOGIN_FAIL:
      return { ...state, login: false };
    default:
      return state;
  }
};
export default combineReducers({
  login
});
