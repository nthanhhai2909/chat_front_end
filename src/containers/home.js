import React from "react";
import Home from "../components/home";
import { authentication } from "../config/auth";
class HomeContanier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  async componentDidMount() {
    const { history } = this.props;
    if (!(await authentication())) {
      history.push("/login");
    }
  }
  render() {
    return <Home />;
  }
}

export default HomeContanier;
