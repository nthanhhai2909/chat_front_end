import * as CONTANTS from "../contants/contants";
export const getToken = () => {
  return localStorage.getItem(CONTANTS.TOKEN);
};

export const setToken = token => {
  localStorage.setItem(CONTANTS.TOKEN, token);
};

export const getIdUser = () => {
  return localStorage.getItem(CONTANTS.ID_USER);
};

export const setIdUser = _id => {
  localStorage.setItem(CONTANTS.ID_USER, _id);
};
