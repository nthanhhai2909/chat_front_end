import { REQUEST_WITH_AUTHENTICATION } from "./request";
import storage from "./storage";
export const authentication = async () => {
  let res;
  try {
    res = await REQUEST_WITH_AUTHENTICATION.GET("/auth", {});
  } catch (err) {
    return false;
  }
  return true;
};
